package com.lindico.repayment.service;

import java.util.List;

import com.lindico.repayment.domain.BorrowerPayents;
import com.lindico.repayment.domain.LoanDetails;
import com.lindico.repayment.exception.ServiceProcessingException;

public interface RepaymentService {

	/**
	 * Calculate and returns the Monthly Payment details. 
	 * @param loandetails
	 * @return
	 * @throws ServiceProcessingException
	 */
	public List<BorrowerPayents> repaymentCalcuation(LoanDetails loandetails) throws ServiceProcessingException;

}
