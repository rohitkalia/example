package com.lindico.repayment.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lindico.repayment.domain.BorrowerPayents;
import com.lindico.repayment.domain.LoanDetails;
import com.lindico.repayment.exception.ServiceProcessingException;
import com.lindico.repayment.service.RepaymentService;
import com.lindico.repayment.utils.LoanCalcuationUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RepaymentServiceImpl implements RepaymentService {

	@Autowired
	LoanCalcuationUtils loanCalcuationUtils;

	
	@Override	
	public List<BorrowerPayents> repaymentCalcuation(LoanDetails loandetails) throws ServiceProcessingException {

		BigDecimal initialOutstandingPrincipal = loandetails.getLoanAmount();
		List<BorrowerPayents> borrowerPayents = new ArrayList<BorrowerPayents>();
		LocalDate startDate = loandetails.getStartDate();
		log.info("Starting Calculation");
		
		// Loop for each month
		for (int i = 0; i < loandetails.getDuration(); i++) {
			//Calculating Annuity			
			BigDecimal annuity = loanCalcuationUtils.calculateAnnuity(initialOutstandingPrincipal,
					loandetails.getDuration() - i, loandetails.getNominalRate());
			//Calculating monthly payment
			BorrowerPayents bp = calculateMonthlyPayment(initialOutstandingPrincipal, loandetails.getNominalRate(),
					annuity);
			initialOutstandingPrincipal = bp.getRemainingOutstandingPrincipal();
			
			bp.setDate(startDate.withDayOfMonth(1));
			borrowerPayents.add(bp);
			startDate = startDate.plusMonths(1);
		}
		return borrowerPayents;
	}

	private BorrowerPayents calculateMonthlyPayment(BigDecimal initialOutstandingPrincipal, BigDecimal nominalRate,
			BigDecimal annuity) {
		BorrowerPayents borrowerPayent = new BorrowerPayents();
		// Calculate Monthly interest
		BigDecimal calcualtedInterest = loanCalcuationUtils.calculateInterest(nominalRate, initialOutstandingPrincipal);
		// Calculate Monthly principal
		BigDecimal calculatedPrinicpal = annuity.subtract(calcualtedInterest);
		if(calculatedPrinicpal.compareTo(initialOutstandingPrincipal)>1) {
			borrowerPayent.setPrincipal(initialOutstandingPrincipal);
		}else {
			borrowerPayent.setPrincipal(calculatedPrinicpal);
		}
		// Calculate Remaining Outstanding Principal		
		borrowerPayent.setRemainingOutstandingPrincipal(initialOutstandingPrincipal.subtract(calculatedPrinicpal));
		borrowerPayent.setInitialOutstandingPrincipal(initialOutstandingPrincipal);
		borrowerPayent.setInterest(calcualtedInterest);
		borrowerPayent.setBorrowerPayents(annuity);	

		return borrowerPayent;
	}

}
