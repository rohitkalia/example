package com.lindico.repayment.exception;

public enum LindicoExceptionEnum {

	/** functional exception */
	L_INVALID_LOAN_DATA("4001", "One or multiple field in the request you are passing is either missing or invalid. "),

	/** technical exception */
	L_EX_GENERIC_ERROR("5000", "LENDICO MS - Generic technical error in Lendico MS call "),
	L_EX_RESOURCE_ACCESS_EXCEPTION("5001", "Lendico MS - Technical Error - Resource access exception");

	private String errorCode;
	private String errorMessagePrefix;

	LindicoExceptionEnum(String errorCode, String errorMessagePrefix) {
		this.errorCode = errorCode;
		this.errorMessagePrefix = errorMessagePrefix;
	}

	public String findErrorCode() {
		return String.valueOf(errorCode);
	}

	public String findErrorMessage() {
		return String.valueOf(errorMessagePrefix);
	}
}
