package com.lindico.repayment.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoanDetails {

	@NotNull(message = "Loan Amount is requried")
	@PositiveOrZero
	private BigDecimal loanAmount;
	@NotNull(message = "Rate of interest is requried")
	@Positive
	private BigDecimal nominalRate;
	@NotNull(message = "Duration is required")
	private Integer duration;
	@NotNull(message = "Start Date is required")
	private LocalDate startDate;
}
