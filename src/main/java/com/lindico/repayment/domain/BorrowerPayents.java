package com.lindico.repayment.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;

@Data
public class BorrowerPayents {
	
	private BigDecimal  borrowerPayents;	
	private LocalDate date;	
	private BigDecimal  initialOutstandingPrincipal;	
	private BigDecimal   interest;
	private BigDecimal  principal;
	private BigDecimal  remainingOutstandingPrincipal;

}
