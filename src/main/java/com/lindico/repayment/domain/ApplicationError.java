package com.lindico.repayment.domain;

import java.io.Serializable;
import lombok.Data;

@Data
public class ApplicationError implements Serializable {
    private static final long serialVersionUID = 1L;    
    private String exceptionName = null;
    private String errorCode = null;
    private String errorMessage = null;
    
}
