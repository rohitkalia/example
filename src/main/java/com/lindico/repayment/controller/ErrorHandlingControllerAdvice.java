package com.lindico.repayment.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.lindico.repayment.domain.ApplicationError;
import com.lindico.repayment.exception.LindicoExceptionEnum;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Setter
@Slf4j
public class ErrorHandlingControllerAdvice {
	public ErrorHandlingControllerAdvice() {
		log.info("construct error handling controller advice");
	}

	@ExceptionHandler
	public ResponseEntity<ApplicationError> handleAllExceptions(Throwable throwable) {
		log.error("handleAllExceptions", throwable);
		ApplicationError error = new ApplicationError();
		error.setExceptionName("Service processing Exception");
		if (throwable instanceof MethodArgumentNotValidException) {
			error.setErrorCode(LindicoExceptionEnum.L_INVALID_LOAN_DATA.findErrorCode());
			error.setErrorMessage(LindicoExceptionEnum.L_INVALID_LOAN_DATA.findErrorMessage());
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}
		error.setErrorCode(LindicoExceptionEnum.L_EX_GENERIC_ERROR.findErrorCode());
		error.setErrorMessage(LindicoExceptionEnum.L_EX_GENERIC_ERROR.findErrorMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
