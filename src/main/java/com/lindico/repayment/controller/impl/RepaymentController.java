package com.lindico.repayment.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lindico.repayment.domain.BorrowerPayents;
import com.lindico.repayment.domain.LoanDetails;
import com.lindico.repayment.exception.ServiceProcessingException;
import com.lindico.repayment.service.RepaymentService;

@Controller
@Validated
@RequestMapping(value = "/")
public class RepaymentController {

	@Autowired
	RepaymentService repaymentService;

	@PostMapping(value = "/generate-plan")
	public ResponseEntity<List<BorrowerPayents>> calcuate(@Valid @RequestBody LoanDetails loandetails)
			throws JsonProcessingException, ServiceProcessingException {

		return ResponseEntity.status(HttpStatus.OK).body(repaymentService.repaymentCalcuation(loandetails));

	}

}
