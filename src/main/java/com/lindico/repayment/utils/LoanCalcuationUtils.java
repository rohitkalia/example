package com.lindico.repayment.utils;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class LoanCalcuationUtils {

	private static int DAYSINMONTH = 30;
	private static int DAYSINYEAR = 360;
	private static int MONTHS = 12;

	/**
	 * Calculates the interest  (Rate * Days in Month * Initial Outstanding Principal) / Days in Year
	 * @param interestRate
	 * @param outstandingPrincipalAmount
	 * @return
	 */
	public BigDecimal calculateInterest(BigDecimal interestRate, BigDecimal outstandingPrincipalAmount) {			
		BigDecimal calcuLatedinterestRate = (interestRate.divide(new BigDecimal(100))
					.multiply(new BigDecimal(DAYSINMONTH).multiply(outstandingPrincipalAmount)));
			return calcuLatedinterestRate.divide(new BigDecimal(DAYSINYEAR), 2, BigDecimal.ROUND_HALF_EVEN);		
	}

	/**
	 * Calculates the monthly interest ( anualRateOfInterest / 12) 
	 * @param anualRateOfInterest
	 * @return
	 */
	public BigDecimal calculateMonthlyRateOfInterest(BigDecimal anualRateOfInterest) {
		return anualRateOfInterest.divide(new BigDecimal(MONTHS), 3, BigDecimal.ROUND_HALF_EVEN);
	}

	/**
	 * Calculates Annuity
	 * @param presentValue
	 * @param months
	 * @param anualRate
	 * @return
	 */
	public BigDecimal calculateAnnuity(BigDecimal presentValue, Integer months, BigDecimal anualRate) {
		double monthlyRate = calculateMonthlyRateOfInterest(anualRate).doubleValue();

		double annuity = (((((monthlyRate) * (months))) == (0))) ? (0)
				: (((((((monthlyRate) * (0.01))) * (presentValue.doubleValue())))
						/ (((1) - (Math.pow((((1) + (((monthlyRate) * (0.01))))), (((months) * (-1)))))))));

		return new BigDecimal(annuity).setScale(2, BigDecimal.ROUND_DOWN);

	}

}
