package com.lindico.repayment.controller;

import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lindico.repayment.controller.impl.RepaymentController;
import com.lindico.repayment.domain.LoanDetails;
import com.lindico.repayment.service.RepaymentService;

@WebMvcTest(RepaymentController.class)
public class RepaymentControllerTest {

	@Autowired
	MockMvc mockMvc;
	@MockBean
	RepaymentService repaymentService;
	
    ObjectMapper objectMapper = new ObjectMapper();

	@Test
	void testControllerWithAllAttributes() {
		try {			
			String  json = "{\"loanAmount\": \"5000\",\"nominalRate\": \"5.0\",\"duration\": 24,\"startDate\": \"2018-01-01T00:00:01Z\"}";			
			mockMvc.perform(post("/generate-plan").content(json).contentType(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk());
		} catch (Exception e) {
			fail("exceptions");
		}
	}
	
	
	@Test
	void testControllerWithMissingAttribute() {
		try {
			LoanDetails loanDetails = LoanDetails.builder().duration(12).loanAmount(new BigDecimal(5000)).nominalRate(new BigDecimal(5)).build();
			String  json = objectMapper.writeValueAsString(loanDetails );			
			mockMvc.perform(post("/generate-plan").content(json).contentType(MediaType.APPLICATION_JSON))
					.andExpect(status().isBadRequest());
		} catch (Exception e) {
			fail("exceptions");
		}
	}

}
