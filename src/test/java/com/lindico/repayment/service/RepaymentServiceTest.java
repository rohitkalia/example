package com.lindico.repayment.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.lindico.repayment.domain.BorrowerPayents;
import com.lindico.repayment.domain.LoanDetails;
import com.lindico.repayment.exception.ServiceProcessingException;
import com.lindico.repayment.utils.LoanCalcuationUtils;

@SpringBootTest
public class RepaymentServiceTest {

	@Autowired
	LoanCalcuationUtils loanCalcuationUtils;

	@Autowired
	RepaymentService repaymentService;

	@Test
	public void test1() {
		LoanDetails loandetails = LoanDetails.builder().loanAmount(new BigDecimal(5000))
				.nominalRate(new BigDecimal(4.00)).startDate(LocalDate.now()).duration(12).build();
		try {
			List<BorrowerPayents> listBorrowerPayents = repaymentService.repaymentCalcuation(loandetails);
			assertEquals(12, listBorrowerPayents.size());

		} catch (ServiceProcessingException e) {
			fail("exceptions");
		}

	}

}
