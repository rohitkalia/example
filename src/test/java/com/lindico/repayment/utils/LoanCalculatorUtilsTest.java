package com.lindico.repayment.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LoanCalculatorUtilsTest {

	@Autowired
	LoanCalcuationUtils loanCalcuationUtils;

	@Test
	public void testInterestRate() {
		BigDecimal rate = loanCalcuationUtils.calculateInterest(new BigDecimal(5.00), new BigDecimal(5000.00));
		assertEquals(new BigDecimal(20.83).setScale(2,BigDecimal.ROUND_HALF_EVEN), rate);
	}	


	@Test
	public void testMonthlyInterestRate() {
		BigDecimal rate = loanCalcuationUtils.calculateMonthlyRateOfInterest(new BigDecimal(5.000));
		assertEquals(new BigDecimal(0.417).setScale(3,BigDecimal.ROUND_HALF_EVEN), rate);
	}
	
	@Test
	public void testAunnutiyCalculation() {
		BigDecimal rate = loanCalcuationUtils.calculateAnnuity(new BigDecimal(5000), 24, new BigDecimal(5.00));
		assertEquals(new BigDecimal(219.36).setScale(2,BigDecimal.ROUND_HALF_EVEN), rate);
	}	
	


}
