package com.lindico.repayment.integrationtest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lindico.repayment.domain.BorrowerPayents;
import com.lindico.repayment.domain.LoanDetails;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RepaymentIntegrationTest {

	@Autowired
	TestRestTemplate testRestTempalte;

	ObjectMapper objectMapper = new ObjectMapper();

	@Test
	void intTest() {

		LoanDetails loanDetails = LoanDetails.builder().duration(1).loanAmount(new BigDecimal(5000))
				.nominalRate(new BigDecimal(5)).startDate(LocalDate.of(2020, 01, 01)).build();

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<LoanDetails> requestEntity = new HttpEntity<>(loanDetails, httpHeaders);
		ResponseEntity<BorrowerPayents[]> reponse = testRestTempalte.exchange("/generate-plan", HttpMethod.POST,
				requestEntity, BorrowerPayents[].class);
		assertEquals(reponse.getStatusCode(), HttpStatus.OK);
	}

}
